/*==========================================================================
 *
 * My modifications are licensed under the GPL2 and are
 * Copyright 2014 Tormod Volden
 *
 * Note that I use the display in landscape mode
 *
 * Based on code from Elecfreaks, retrieved from
 * http://www.elecfreaks.com/wiki/index.php?title=2.2S%22_TFT_LCD:_TFT01-2.2S
 *
 *  Original comments:

The LCD connection is the same as Nokia LCD5110 and  is a "8 Bit Pant Demo"

Just for ElecFreaks TFT01-2.2SP, which use SPI serial port and 240x320 pixel.
The driver is ILI9341.
 
by Elecfreaks

==========================================================================*/ 

/* using ST library defines */
# include <stm32f10x_rcc.h>
# include <stm32f10x_gpio.h>
# include "spi.h"
# include "glcdfont.h"
# include "ILI9341.h"

#define LCD_MAX_X 320
#define LCD_MAX_Y 240

#define LCD_SPI SPI1

/* SCK and MOSI bit-banging on same pins as SPI1 */
#ifdef LCD_BB
/* SCK on Maple Mini D6 */
# define SCK_LO  GPIO_ResetBits(GPIOA, GPIO_Pin_5)
# define SCK_HI  GPIO_SetBits  (GPIOA, GPIO_Pin_5)
/* MOSI on Maple Mini D4 */
# define MOSI_LO GPIO_ResetBits(GPIOA, GPIO_Pin_7)
# define MOSI_HI GPIO_SetBits  (GPIOA, GPIO_Pin_7)
#endif

/* CS on Maple Mini D7 */
#define CS_LO   GPIO_ResetBits(GPIOA, GPIO_Pin_4)
#define CS_HI   GPIO_SetBits  (GPIOA, GPIO_Pin_4)

/* D/C (bit9) on Maple Mini D8 */
#define DC_LO   GPIO_ResetBits(GPIOA, GPIO_Pin_3)
#define DC_HI   GPIO_SetBits  (GPIOA, GPIO_Pin_3)

/* RST on Maple Mini D9 */
#define RST_LO  GPIO_ResetBits(GPIOA, GPIO_Pin_2)
#define RST_HI  GPIO_SetBits  (GPIOA, GPIO_Pin_2)


void LCD_GPIO_Config()
{
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd ( RCC_APB2Periph_GPIOA, ENABLE );

	GPIO_SetBits(GPIOA, GPIO_Pin_2);	/* disable RST */
	GPIO_SetBits(GPIOA, GPIO_Pin_4);	/* set CS inactive */

        /* LCD pins on GPIOA 2,3,4,5,7 */
	GPIO_StructInit (&GPIO_InitStructure );
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4; /* MOSI */
	GPIO_InitStructure.GPIO_Pin |= GPIO_Pin_2 | GPIO_Pin_3; /* D/C and RST */
	GPIO_InitStructure.GPIO_Pin |=  GPIO_Pin_5 | GPIO_Pin_7; /* bit-bang */
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz ;
	GPIO_Init (GPIOA, &GPIO_InitStructure );
}

void LCD_Writ_Bus(char data)
{
  unsigned char c;
  int d;

  CS_LO;
#ifdef LCD_BB
  for (c=0; c<8; c++)
  {
	if (data & 0x80)
		MOSI_HI;
	else
		MOSI_LO;
	data = data<<1;
	SCK_LO;
	asm ("nop");
	SCK_HI;
  }
#else
  spiReadWrite(SPI1, 0, &data, 1, SPI_MEDIUM);
#endif
  CS_HI;
}
 
void LCD_Write_COM(char VL)  
{   
  DC_LO;
  LCD_Writ_Bus(VL);
}
 
void LCD_Write_DATA(char VL)    
{
  DC_HI;
  LCD_Writ_Bus(VL);
}
 
void LCD_Address_Set(unsigned int x1,unsigned int y1,unsigned int x2,unsigned int y2)
{
   LCD_Write_COM(0x2a);
   LCD_Write_DATA(x1>>8);
   LCD_Write_DATA(x1);
   LCD_Write_DATA(x2>>8);
   LCD_Write_DATA(x2);
  
   LCD_Write_COM(0x2b);
   LCD_Write_DATA(y1>>8);
   LCD_Write_DATA(y1);
   LCD_Write_DATA(y2>>8);
   LCD_Write_DATA(y2);

   LCD_Write_COM(0x2C);         				 
}
 
void LCD_Init(void)
{
 
        RST_LO;
        Delay(1);	/* 10 us is needed */
        RST_HI;
        Delay(1);	/* some delay is needed ! */
  
        LCD_Write_COM(0xCB);  
        LCD_Write_DATA(0x39); 
        LCD_Write_DATA(0x2C); 
        LCD_Write_DATA(0x00); 
        LCD_Write_DATA(0x34); 
        LCD_Write_DATA(0x02); 

        LCD_Write_COM(0xCF);  
        LCD_Write_DATA(0x00); 
        LCD_Write_DATA(0XC1); 
        LCD_Write_DATA(0X30); 

        LCD_Write_COM(0xE8);  
        LCD_Write_DATA(0x85); 
        LCD_Write_DATA(0x00); 
        LCD_Write_DATA(0x78); 

        LCD_Write_COM(0xEA);  
        LCD_Write_DATA(0x00); 
        LCD_Write_DATA(0x00); 
 
        LCD_Write_COM(0xED);  
        LCD_Write_DATA(0x64); 
        LCD_Write_DATA(0x03); 
        LCD_Write_DATA(0X12); 
        LCD_Write_DATA(0X81); 

        LCD_Write_COM(0xF7);  
        LCD_Write_DATA(0x20); 
  
        LCD_Write_COM(0xC0);    //Power control 
        LCD_Write_DATA(0x23);   //VRH[5:0] 
 
        LCD_Write_COM(0xC1);    //Power control 
        LCD_Write_DATA(0x10);   //SAP[2:0];BT[3:0] 

        LCD_Write_COM(0xC5);    //VCM control 
        LCD_Write_DATA(0x3e);   //Contrast
        LCD_Write_DATA(0x28); 
 
        LCD_Write_COM(0xC7);    //VCM control2 
        LCD_Write_DATA(0x86);   //--
 
        LCD_Write_COM(0x36);    // Memory Access Control 
        //LCD_Write_DATA(0x48);   //C8	   //48 68竖屏//28 E8 横屏
        LCD_Write_DATA(0x08);

        LCD_Write_COM(0x3A);    
        LCD_Write_DATA(0x55); 

        LCD_Write_COM(0xB1);    
        LCD_Write_DATA(0x00);  
        LCD_Write_DATA(0x18); 
 
        LCD_Write_COM(0xB6);    // Display Function Control 
        LCD_Write_DATA(0x08); 
        LCD_Write_DATA(0x82);
        LCD_Write_DATA(0x27);  
/* 
        LCD_Write_COM(0xF2);    // 3Gamma Function Disable 
        LCD_Write_DATA(0x00); 
 
        LCD_Write_COM(0x26);    //Gamma curve selected 
        LCD_Write_DATA(0x01); 

        LCD_Write_COM(0xE0);    //Set Gamma 
        LCD_Write_DATA(0x0F); 
        LCD_Write_DATA(0x31); 
        LCD_Write_DATA(0x2B); 
        LCD_Write_DATA(0x0C); 
        LCD_Write_DATA(0x0E); 
        LCD_Write_DATA(0x08); 
        LCD_Write_DATA(0x4E); 
        LCD_Write_DATA(0xF1); 
        LCD_Write_DATA(0x37); 
        LCD_Write_DATA(0x07); 
        LCD_Write_DATA(0x10); 
        LCD_Write_DATA(0x03); 
        LCD_Write_DATA(0x0E); 
        LCD_Write_DATA(0x09); 
        LCD_Write_DATA(0x00); 

        LCD_Write_COM(0XE1);    //Set Gamma 
        LCD_Write_DATA(0x00); 
        LCD_Write_DATA(0x0E); 
        LCD_Write_DATA(0x14); 
        LCD_Write_DATA(0x03); 
        LCD_Write_DATA(0x11); 
        LCD_Write_DATA(0x07); 
        LCD_Write_DATA(0x31); 
        LCD_Write_DATA(0xC1); 
        LCD_Write_DATA(0x48); 
        LCD_Write_DATA(0x08); 
        LCD_Write_DATA(0x0F); 
        LCD_Write_DATA(0x0C); 
        LCD_Write_DATA(0x31); 
        LCD_Write_DATA(0x36); 
        LCD_Write_DATA(0x0F); 
*/
        LCD_Write_COM(0x11);    //Exit Sleep 
        Delay(120); 
				
        LCD_Write_COM(0x29);    //Display on 
        LCD_Write_COM(0x2c);   
}
 
void LCD_Paint_Rect(int x, int y, int w, int h, char VH, char VL)
{
  int i,j;
  LCD_Address_Set(y,x,y+h-1,x+w-1);
  for (i=0; i<w; i++)
  {
    for (j=0;j<h;j++)
    {
      LCD_Write_DATA(VH);
      LCD_Write_DATA(VL);
    }
  }
}

void LCD_Color_Bars_Demo()
{
  LCD_Paint_Rect(  0,0,32,128,0xFF,0xFF);
  LCD_Paint_Rect( 32,0,32,128,0xF0,0xF0);
  LCD_Paint_Rect( 64,0,32,128,0xE0,0xE0);
  LCD_Paint_Rect( 96,0,32,128,0x05,0x05);
  LCD_Paint_Rect(128,0,32,128,0x1F,0x1F);
  LCD_Paint_Rect(160,0,32,128,0x00,0x00);
  LCD_Paint_Rect(192,0,32,128,0xFF,0x05);
  LCD_Paint_Rect(224,0,32,128,0xE0,0x15);
  LCD_Paint_Rect(256,0,32,128,0x1F,0x15);
  LCD_Paint_Rect(288,0,32,128,0x80,0x80);
}

void LCD_draw_char(int x, int y, char ch, int fg, int bg)
{
	int i,j;

	LCD_Address_Set(y,x,y+7,x+5);
	for (i = 0; i < 6; i++) {
		for (j = 0; j < 8; j++) {
			if ( i != 5 && ASCII[(unsigned char) ch * 5 + i] & (1 << j)) {
			      LCD_Write_DATA(fg >> 8);
			      LCD_Write_DATA(fg & 0xFF);
			} else {
			      LCD_Write_DATA(bg >> 8);
			      LCD_Write_DATA(bg & 0xFF);
			}
		}
	}
}

void LCD_draw_str(int x, int y, char *string, int fg, int bg)
{
	int xp = x;
	int yp = y;
	char *strp = string;

	while (*strp) {
		LCD_draw_char(xp, yp, *strp, fg, bg);
		strp++;
		xp += 6;
		if (xp > LCD_MAX_X - 6) {
			xp = 0;
			yp += 10;
		}
	}
}
