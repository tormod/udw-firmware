
#define LCD_MAX_X 320
#define LCD_MAX_Y 240
#define CHARW 6
#define CHARH 8
#define CONSOLE_WIDTH 51
#define CONSOLE_HEIGHT 30

void console_vert_scroll(int lines);
void console_erase_eol();
void console_init();
int console_print(char *string);
void console_print_char(char c);

