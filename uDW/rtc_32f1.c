/*------------------------------------------------------------------------/
/  STM32F100 RTC control module
/-------------------------------------------------------------------------/
/
/  Copyright (C) 2013, ChaN, all right reserved.
/
/ * This software is a free software and there is NO WARRANTY.
/ * No restriction on use. You can use, modify and redistribute it for
/   personal, non-profit or commercial products UNDER YOUR RESPONSIBILITY.
/ * Redistributions of source code must retain the above copyright notice.
/
/  Modifications to use ST library functions and constants:
/  Copyright (C) 2014-2015 Tormod Volden
/
/-------------------------------------------------------------------------*/

#include <stm32f10x.h>
#include <stm32f10x_pwr.h>
#include <stm32f10x_rcc.h>
#include <stm32f10x_rtc.h>
#include <stm32f10x_bkp.h>
#include "rtc_32f1.h"

#define F_LSE	32768	/* LSE oscillator frequency */


static
const uint8_t samurai[] = {31,28,31,30,31,30,31,31,30,31,30,31};

static
uint8_t rtcok;

#if 0
extern __IO uint32_t soft_clock;
#endif

/*------------------------------------------*/
/* Initialize RTC                           */
/*------------------------------------------*/

int rtc_initialize (int reset)	/* 1:RTC is available, 0:RTC is not available */
{
	uint32_t n;

	/* Enable BKP and PWR module */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);

	// PWR_BackupAccessCmd(ENABLE);
	PWR->CR |= PWR_CR_DBP;	/* Enable write access to backup domain */

	/* Reset Backup Domain */
	/* sets BDCR_BDRST_BB to 1 then 0 */
	if (reset)
		BKP_DeInit();

#if 1
	RCC->BDCR = RCC_BDCR_RTCEN | RCC_BDCR_RTCSEL_LSE | RCC_BDCR_LSEON;	/* Enable LSE oscillator */
#else
	/* Waits until the RTC registers (RTC_CNT, RTC_ALR and RTC_PRL) */
	/* are synchronized with RTC APB clock. */
	/* This function must be called before any read operation after an APB reset or an APB clock stop */
	RTC_WaitForSynchro();

	RCC_LSEConfig(RCC_LSE_ON);
	while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET) {}

	RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);

	/* Enables or disables the RTC clock. */
	/* This function must be used only after the RTC clock was selected using the RCC_RTCCLKConfig function. */
	RCC_RTCCLKCmd(ENABLE);

	RTC_WaitForSynchro();

	/* Waits until last write operation on RTC registers has finished. */
	/* This function must be called before any write to RTC registers. */
	RTC_WaitForLastTask();

	RTC_SetPrescaler(F_LSE - 1);
	RTC_WaitForLastTask();
#endif

	for (n = 8000000; n && !(RCC->BDCR & RCC_BDCR_LSERDY); n--) ;		/* Wait for LSE start and stable */
	if (n) {
		for (n = 100000; n && !(RTC->CRL & RTC_FLAG_RTOFF); n--) ;	/* rtc_waitForLastTask() */
		if (n) {
			RTC->CRL = RTC_CRL_CNF;                 /* Enter RTC configuration mode */
			RTC->PRLH = 0; RTC->PRLL = F_LSE - 1;	/* Set RTC clock divider for 1 sec tick */
			RTC->CRL = 0;				/* Exit RTC configuration mode and clear RSF */
			for ( ; n && !(RTC->CRL & RTC_FLAG_RTOFF); n--) ;	/* Wait for RTC internal process rtc_waitForLastTask() */
			for ( ; n && !(RTC->CRL & RTC_FLAG_RSF); n--) ;	/* Wait for RTC is in sync */ /* RTC_WaitForSynchro() */
		}
	}

	if (n) {
		rtcok = 1;		/* RTC is available */
	} else {
		rtcok = 0;		/* RTC is not available */
		RCC->BDCR = 0;	/* Stop LSE oscillator */
	}

	// PWR_BackupAccessCmd(DISABLE);
	PWR->CR &= ~PWR_CR_DBP;	/* Inhibit write access to backup domain */

	return rtcok;
}



/*------------------------------------------*/
/* Set time in UTC                          */
/*------------------------------------------*/

int rtc_setutc (uint32_t tmr)
{
	uint32_t n = 0;

#if 0
	soft_clock = tmr;
	return 0;

#else
	if (rtcok) {
		PWR->CR |= PWR_CR_DBP;		/* Enable write access to backup domain */
		for (n = 100000; n && !(RTC->CRL & RTC_CRL_RTOFF); n--) ;	/* Wait for end of RTC internal process */
		if (n) {
			RTC->CRL = RTC_CRL_CNF;		/* Enter RTC configuration mode */
			RTC->CNTL = tmr; RTC->CNTH = tmr >> 16;	/* Set time counter */
			RTC->PRLL = F_LSE - 1; RTC->PRLH = 0; 	/* Set RTC clock divider for 1 sec tick */
			RTC->CRL = 0;			/* Exit RTC configuration mode and clear RSF */
			for ( ; n && !(RTC->CRL & RTC_CRL_RTOFF); n--) ;	/* Wait for end of RTC internal process */
		}
		PWR->CR &= ~PWR_CR_DBP;		/* Inhibit write access to backup domain */
	}

	return n ? 1 : 0;
#endif
}



/*------------------------------------------*/
/* Get time in UTC                          */
/*------------------------------------------*/

int rtc_getutc (uint32_t* tmr)
{
	uint32_t t1, t2;

#if 0
	*tmr = soft_clock;
	return 1;

#else
	if (rtcok) {
		/* Read RTC counter */
		t1 = RTC->CNTH << 16 | RTC->CNTL;
		do {
			t2 = t1;
			t1 = RTC->CNTH << 16 | RTC->CNTL;	
		} while (t1 != t2);
		*tmr = t1;
		return 1;
	}
	return 0;
#endif
}



/*------------------------------------------*/
/* Get time in calendar form                */
/*------------------------------------------*/

int rtc_gettime (RTCTIME* rtc)
{
	uint32_t utc, n, i, d;

	if (!rtc_getutc(&utc)) return 0;

	utc += (long)(_RTC_TDIF * 3600);

	rtc->sec = (uint8_t)(utc % 60); utc /= 60;
	rtc->min = (uint8_t)(utc % 60); utc /= 60;
	rtc->hour = (uint8_t)(utc % 24); utc /= 24;
	rtc->wday = (uint8_t)((utc + 4) % 7);
	rtc->year = (uint16_t)(1970 + utc / 1461 * 4); utc %= 1461;
	n = ((utc >= 1096) ? utc - 1 : utc) / 365;
	rtc->year += n;
	utc -= n * 365 + (n > 2 ? 1 : 0);
	for (i = 0; i < 12; i++) {
		d = samurai[i];
		if (i == 1 && n == 2) d++;
		if (utc < d) break;
		utc -= d;
	}
	rtc->month = (uint8_t)(1 + i);
	rtc->mday = (uint8_t)(1 + utc);

	return 1;
}



/*------------------------------------------*/
/* Set time in calendar form                */
/*------------------------------------------*/

int rtc_settime (const RTCTIME* rtc)
{
	uint32_t utc, i, y;


	y = rtc->year - 1970;
	if (y > 2106 || !rtc->month || !rtc->mday) return 0;

	utc = y / 4 * 1461; y %= 4;
	utc += y * 365 + (y > 2 ? 1 : 0);
	for (i = 0; i < 12 && i + 1 < rtc->month; i++) {
		utc += samurai[i];
		if (i == 1 && y == 2) utc++;
	}
	utc += rtc->mday - 1;
	utc *= 86400;
	utc += rtc->hour * 3600 + rtc->min * 60 + rtc->sec;

	utc -= (long)(_RTC_TDIF * 3600);

	return rtc_setutc(utc);
}


