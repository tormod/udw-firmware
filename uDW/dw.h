/*
   DriveWire server implementation for uDW
   - relatively hardware independent part

   Copyright 2014 Tormod Volden

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

int send_io(int out);
int read_io(int *in);
int list_dir(char *dirname);
int dw_server(USART_TypeDef *uart_in);

#define READ_TIMEOUT 100

#define BAUD_COCO1   38400
#define BAUD_DEFAULT 57600
#define BAUD_COCO3  115200
